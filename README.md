## 기본 설정

### npm start

npm start를 통해서 서버 실행!!
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### npm install -g create-react-app

설치되어 있으면, 생략
we want to use the create-react-app CLI tool becaouse it registers optimal settings and serveral benefits such as Hot reloading and service workers.

### creat-react-app sample-frontend

sample=frontend 프로젝트 생성
babel & webpack등 기본적으로 설치됨

### axios

axios 모듈을 통한 웹서버와의 통신
[https://github.com/axios/axios]
[https://www.npmjs.com/package/axios-jsonp-pro]

### semantic-ui-react, semantic-ui-css

semantic-ui 추가[https://react.semantic-ui.com/usage]


### redux, react-redux, redux-actions

리덕스 관련 라이브러리

### cross-env

환경변수를 모든 운영체제에서 호환되는 형태로 설정해주는 라이브러리
import React, { Component } from 'react';
import CounterContainer from 'tutorial/redux/containers/CounterContainer';
import TodosContainer from 'tutorial/redux/containers/TodosContainer';
import AppTemplate from './AppTemplate';

class App extends Component {
  render() {
    return (
      <AppTemplate
        counter={<CounterContainer />}
        todos={<TodosContainer />}
      />
    );
  }
}

export default App;
import React from 'react';
import { BrowserRouter, Route } from 'react-router-dom';
import UserApp from 'pago/shared/UserApp';
import {Login } from 'pago/pages';

const Root = () => (
    <BrowserRouter>
      <div>
        <Route exact path="/" component={Login} />
        <Route path="/users" component={UserApp} />
      </div>
    </BrowserRouter>
);

export default Root;
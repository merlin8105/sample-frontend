
import React from 'react';
import { NavLink  } from 'react-router-dom';

const Sidebar = () => {

  const activeStyle= {
    color:'green'
  }

  return (
    <div className="column" id="sidebar">
      <div className="ui secondary vertical fluid menu">
        <div className="item">
          <div className="header">회원 관리</div>
          <div className="menu">
            <NavLink  className="item" activeStyle={activeStyle} exact to="/users">회원 목록</NavLink >
            <NavLink  className="item" activeStyle={activeStyle} exact to="/users/:id">회원 상세</NavLink >
          </div>
        </div>
        <div className="item">
          <div className="header">시그널 관리</div>
          <div className="menu">
            <NavLink  className="item" to="#">기본 정보</NavLink >
            <NavLink  className="item" to="#">시그널 목록</NavLink >
            <NavLink  className="item" to="#">시그널 상세</NavLink >
          </div>
        </div>
        <div className="item">
          <div className="header">텔레그램 체널 관리</div>
          <div className="menu">
            <NavLink  className="item" to="#">텔레그램 체널 목록</NavLink >
            <NavLink  className="item" to="#">텔레그램 체널 상세</NavLink >
          </div>
        </div>
        <div className="item">
          <div className="header">통계 관리</div>
          <div className="menu">
              <NavLink  className="item" to="#">통계 목록</NavLink >
              <NavLink  className="item" to="#">통계 상세</NavLink >
            </div>
        </div>
        <div className="item">
          <div className="header">서버 관리</div>
          <div className="menu">
              <NavLink  className="item" to="#">서버 목록</NavLink >
              <NavLink  className="item" to="#">서버 상세</NavLink >
            </div>
        </div>								
      </div>
    </div>
  );
};

export default Sidebar;
/* eslint-disable jsx-a11y/anchor-is-valid */
import React from 'react';
import 'pago/components/Header.css';
import {Dropdown, Button} from 'semantic-ui-react'; 

const Header = () => {
  return (
    <div className="ui inverted huge borderless fixed fluid menu">
      <a className="header item">PAGO</a>
      <div className="right menu">
        <div className="item">
          <div className="ui small action left icon labeled input" >
            <Dropdown 
              options={[
                {key: 'name', value: 'name', text : '이름'},
                {key: 'email', value: 'email', text : '이메일'},
                {key: 'phone', value: 'phone', text : '전화번호'},
              ]}
              placeholder='선택'
              selection
              
            />
            <input placeholder="Search..." type="text"/>
            <Button as='a' color='teal' >
              Search
            </Button>
          </div>
          <a className="ui inverted button" >Log out</a>
        </div>
      </div>
    </div>
  );
};

export default Header;
import React, { Component } from 'react';
import {
  Image
} from 'semantic-ui-react';

class User extends Component {
  shouldComponentUpdate(prevProps, prevState) {
    return this.props.user !== prevProps.user;
  }
  render() {
    // const { username } = this.props.user;
    
    // console.log('%s가 렌더링 되고있어요!!!', username);

    return (
      <div className="item">
        <div className="ui small image">
          <a className="ui corner red label"><i class="heart icon"></i></a>
          <Image src='https://react.semantic-ui.com/images/wireframe/image.png' />
          <div className="extra">
            <div className="ui star rating" data-rating="4"><i class="icon active"></i><i class="icon active"></i><i class="icon "></i><i class="icon "></i></div>
          </div>
        </div>
        <div className="content">
          <p className="header">홍길동</p>
          <div className="meta">
            <p>pago@naver.com</p>
            <p>010-1111-2222</p>
          </div>
          <div claclassNamess="extra">
            <div className="ui right floated teal button" >
              회원 상세<i className="right chevron icon"></i>
            </div>
            <div className="ui red label">
              우수 회원
            </div>
            <div className="ui purple label">
                등록된 포지션 : 5개
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default User;
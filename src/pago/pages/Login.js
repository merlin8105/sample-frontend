import React, { Component } from 'react';


class Login extends Component {

  goMainPage(e) {
    e.preventDefault();
    window.location = "http://localhost:3000/users";
  }

  render() {

    return (
      <div className="ui raised very padded center aligned text container segment">
        <h1 className="ui header">Log in</h1>
        <br/>
        <div className="field">
          <div className="ui left icon input">
            <i className="mail outline icon"></i>
            <input type="email" name="email" placeholder="Email Address" autoFocus={true} />
          </div>
        </div>
        <br/>
        <div className="field">
         <div className="ui left icon input">
          <i className="lock icon"></i>
          <input type="password" name="password" placeholder="Password"/>
         </div>
        </div>
        <br/>
        <div className="actions">
          <button className=" ui large teal button" type="submit" onClick={this.goMainPage}>Log in</button>
        </div>
      </div>
    );
  }
}

export default Login;
import React, { Component } from 'react';
import withRequest from 'pago/shared/withRequest';
import {
  Image
} from 'semantic-ui-react';
import { List, Map } from 'immutable';

const UserItem = ({ id, name, email, phoneNo, admin, created, approved }) => (

  <div className="item">
  <div className="ui small image">
    <a className="ui corner red label"><i className="heart icon"></i></a>
    <Image src='https://react.semantic-ui.com/images/wireframe/image.png' />
    <div className="extra">
      <div className="ui star rating" data-rating="4"><i className="icon active"></i><i className="icon active"></i><i className="icon "></i><i className="icon "></i></div>
    </div>
  </div>
  <div className="content">
    <p className="header">{name}</p>
    <div className="meta">
      <p>{email}</p>
      <p>{phoneNo}</p>
    </div>
    <div className="extra">
      <div className="ui right floated teal button" >
        회원 상세<i className="right chevron icon"></i>
      </div>
      <div className="ui red label">
        {admin? '어드민' : '일반'}
      </div>
      <div className="ui purple label">
       {created}
      </div>
    </div>
  </div>
</div>
)


class UserList extends Component {

  render() {
    console.log('UserList 가 렌더링되고 있어요!')
    const { data } = this.props;
    if (!data) return null;

    console.log(JSON.stringify(data._embedded.users[0]))

    const todoItems = data._embedded.users.map(
      user => {
        const { id, name, email, phoneNo, admin, created, approved  } = user;
        return (
          <UserItem
            id={id}
            key={id}
            name={name}
            email={email}
            phoneNo={phoneNo}
            admin={admin}
            created={created}
            approved={approved}
          />
        )
      }
    )

    return (
      <div className="ui grid">
        <div className="row">
          <h1 className="ui huge header">
            회원 목록
          </h1>
        </div>
        <div className="ui divider"></div>
        <div className="row">
          <div className="ui relaxed divided items">
            {todoItems}
          </div>
        </div>
      </div>
    );
  }
}

export default withRequest('get','http://54.180.63.154/api/users')(UserList);

// http://54.180.63.154/api/users
//https://jsonplaceholder.typicode.com/comments?postId=1
import React from 'react';
import { Route, Switch } from 'react-router-dom';
import Header from 'pago/components/Header';
import Sidebar from 'pago/components/Sidebar';
import {UserList, User, Login } from 'pago/pages';

const App = () => {
    return (
      <div>
        <Header />
        <div className="ui grid">
          <div className="row">
            <Sidebar />
            <div className="ui column" id="content">
                <Route exact path="/" component={Login} />
                <Switch>
                {/* <Route path="/users/:id" component={User} /> */}
                  <Route path="/users" component={UserList} />
              </Switch>
            </div>
          </div>
        </div>
      </div> 
    );
};

export default App;
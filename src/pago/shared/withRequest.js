import React, { Component } from 'react';
import axios from 'axios';

const withRequest = (method, url, params={}) => (WrappedComponent) => {
  return class extends Component {

    state = {
      data: null
    }

    mkAxiosObj() {
      let obj = null;
      if (method === 'get') {
        obj = {
          method:method,
          url:url,
          params:params
        }
      } else {
        obj = {
          method:method,
          url:url,
          data:params
        }
      }
      return obj
    }
    
    async initialize() {
      try {
        
        // const response = await axios(this.mkAxiosObj())
        const response = await axios(this.mkAxiosObj())
        this.setState({
          data: response.data
        });
      } catch (e) {
        console.log(e);
      }
    }

    componentDidMount() {
      this.initialize();
    }

    render() {
      const { data } = this.state;
      return (
        <WrappedComponent {...this.props} data={data}/>
      )
    }
  }
}

export default withRequest;
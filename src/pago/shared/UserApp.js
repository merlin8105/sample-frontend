import React from 'react';
import { Route, Switch } from 'react-router-dom';
import Header from 'pago/components/Header';
import Sidebar from 'pago/components/Sidebar';
import {UserList } from 'pago/pages';

const UserApp = ({match}) => {
    return (
      <div>
        <Header />
        <div className="ui grid">
          <div className="row">
            <Sidebar />
            <div className="ui column" id="content">
                {/* <Switch> */}
                {/* <Route path="/users/:id" component={User} /> */}
                  <Route path={`${match.url}`} component={UserList} />
              {/* </Switch> */}
            </div>
          </div>
        </div>
      </div> 
    );
};

export default UserApp;